package com.example.RiskyAreaApis.dto.UserDto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class UserLogInDto {

    @NotEmpty(message = "Email Required")
    private String email;

    @NotEmpty(message = "Password Required")
    private String password;
}
