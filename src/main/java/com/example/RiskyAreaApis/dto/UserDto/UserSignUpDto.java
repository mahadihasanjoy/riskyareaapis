package com.example.RiskyAreaApis.dto.UserDto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class UserSignUpDto {

    @NotEmpty(message = "User Name can't be empty")
    private String name;

    @NotEmpty(message = "User email can't be empty")
    private String email;

    @NotEmpty(message = "User phone number can't be empty")
    private String phoneNumber;

    @NotEmpty(message = "User password can't be empty")
    private String password;

    private String age;

    private String gender;

    private String address;


}
