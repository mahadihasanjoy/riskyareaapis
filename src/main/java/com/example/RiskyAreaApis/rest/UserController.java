package com.example.RiskyAreaApis.rest;

import com.example.RiskyAreaApis.dto.UserDto.UserLogInDto;
import com.example.RiskyAreaApis.dto.UserDto.UserSignUpDto;
import com.example.RiskyAreaApis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@RequestMapping(path = "/rest/user/")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping(path = "/signUp")
    public ResponseEntity<HashMap<String, String>> signUp(@Valid @RequestBody UserSignUpDto userSignUpDto)
    {
        HashMap<String, String> messageMap = new HashMap<>();
        if (userService.userSignUp(userSignUpDto))
        {
            messageMap.put("Message", "User Added");
            return new ResponseEntity<HashMap<String, String>>(messageMap, HttpStatus.OK);
        }
        else
        {
            messageMap.put("Message", "User Email Exists");
            return new ResponseEntity<HashMap<String, String>>(messageMap, HttpStatus.CONFLICT);
        }
    }

    @PostMapping(path = "/logIn")
    public ResponseEntity<HashMap<String, String>> logIn(@Valid @RequestBody UserLogInDto userLogInDto)
    {
        HashMap<String, String> messageMap = new HashMap<>();
       if ( userService.userLogIn(userLogInDto))
       {
           messageMap.put("Message", "Successfully Loged In");
           return new ResponseEntity<HashMap<String, String>>(messageMap, HttpStatus.OK);
       }
       else
       {
           messageMap.put("Message", "Password Doesn't match");
           return new ResponseEntity<HashMap<String, String>>(messageMap, HttpStatus.FORBIDDEN);
       }
    }

}
