package com.example.RiskyAreaApis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RiskyAreaApisApplication {

	public static void main(String[] args) {
		SpringApplication.run(RiskyAreaApisApplication.class, args);
	}

}
