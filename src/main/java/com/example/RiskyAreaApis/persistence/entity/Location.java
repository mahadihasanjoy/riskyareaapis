package com.example.RiskyAreaApis.persistence.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "locationName")
    private String location;

    @Column(name = "latitude")
    private Double latitude;

    @Column (name = "longitude")
    private Double longitude;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "userFrequentLocations",
            joinColumns = { @JoinColumn(name = "locationId") },
            inverseJoinColumns = { @JoinColumn(name = "UserId") }
    )
    List<User> users = new ArrayList<>();

}
