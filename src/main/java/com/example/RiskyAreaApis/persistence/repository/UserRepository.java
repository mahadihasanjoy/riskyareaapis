package com.example.RiskyAreaApis.persistence.repository;

import com.example.RiskyAreaApis.persistence.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(@Param("email") String email);
    boolean existsByEmail(String email);
}
