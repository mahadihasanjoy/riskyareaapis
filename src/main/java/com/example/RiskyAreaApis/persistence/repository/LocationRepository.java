package com.example.RiskyAreaApis.persistence.repository;

import com.example.RiskyAreaApis.persistence.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends JpaRepository<Location,Long> {
}
