package com.example.RiskyAreaApis.service;

import com.example.RiskyAreaApis.dto.UserDto.UserLogInDto;
import com.example.RiskyAreaApis.dto.UserDto.UserSignUpDto;

public interface UserService {
    boolean userSignUp(UserSignUpDto userSignUpDto);
    boolean userLogIn(UserLogInDto userLogInDto);
}
