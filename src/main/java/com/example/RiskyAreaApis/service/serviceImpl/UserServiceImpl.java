package com.example.RiskyAreaApis.service.serviceImpl;

import com.example.RiskyAreaApis.dto.UserDto.UserLogInDto;
import com.example.RiskyAreaApis.dto.UserDto.UserSignUpDto;
import com.example.RiskyAreaApis.helper.dataBuilder.PrepareUserData;
import com.example.RiskyAreaApis.persistence.entity.User;
import com.example.RiskyAreaApis.persistence.repository.UserRepository;
import com.example.RiskyAreaApis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    PrepareUserData prepareUserData;

    @Override
    public boolean userSignUp(UserSignUpDto userSignUpDto) {

        boolean emailExists = userRepository.existsByEmail(userSignUpDto.getEmail());
        if (!emailExists)
        {
            userRepository.save(prepareUserData.prepareSignUpData(userSignUpDto));
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public boolean userLogIn(UserLogInDto userLogInDto) {
        User user = userRepository.findByEmail(userLogInDto.getEmail());
        if(user.getPassword().equals(userLogInDto.getPassword()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
