package com.example.RiskyAreaApis.helper.dataBuilder;

import com.example.RiskyAreaApis.dto.UserDto.UserSignUpDto;
import com.example.RiskyAreaApis.persistence.entity.User;
import org.springframework.stereotype.Component;

@Component
public class PrepareUserData {

    public User prepareSignUpData(UserSignUpDto userSignUpDto)
    {
        User user = new User();
        user.setName(userSignUpDto.getName());
        user.setEmail(userSignUpDto.getEmail());
        user.setPhoneNumber(userSignUpDto.getPhoneNumber());
        user.setPassword(userSignUpDto.getPassword());
        user.setGender(userSignUpDto.getGender());
        user.setAge(userSignUpDto.getAge());
        return user;

    }
}
